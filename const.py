# ------------------------------------------------------------
# _old
# バックテストのデータを取得する期間
data_days = 180
# データベースのパス
db_path = "dbdata\\old_db_" + str(data_days) + ".db"
# ログファイルのパス
log_path = "log\\old_app_" + str(data_days) + ".log"
# 画像ファイルを保存するパス
file_path = "graph\\old\\"
# ティッカーシンボル
symbol="XBTUSD"
# ティッカーシンボル（cryptowatch)
cryptowatch_symbol = "XBTUSD"
# ------------------------------------------------------------
# new
# バックテストのデータを取得する機関
new_data_days = 180
# データベースのパス
new_db_path = "dbdata\\new_db" + str(new_data_days) + ".db"
# ログファイルのパス
new_log_path ="log\\new_app" + str(new_data_days) + ".db"
# 画像ファイル保存するパス
new_file_path = "graph\\new\\"
# ティッカーシンボル(bitmex)
new_bitmex_symbol="XBTUSD"
# ティッカーシンボル(cryptowatch)
new_cryptowatch_symbol = "ETHUSD"

