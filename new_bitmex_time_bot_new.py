# encoding:utf-8
# 統計情報を用いてトレードを行うBOTです。
import const
import os
# データベースとログファイルを削除すうる
if os.path.exists(const.new_db_path):
    os.remove(const.new_db_path)
if os.path.exists(const.new_log_path):
    os.remove(const.new_log_path)

# インポート
import pandas as pd
import requests
import logger_init
import logging
import numpy as np
import matplotlib.pyplot as plt
logger = logging.getLogger(__name__)
import scipy.stats as stats
np.set_printoptions(threshold=np.nan)
from db.db_connect import *
import sys
# 再帰の決定上限1万回
sys.setrecursionlimit(10000)

class GetOhlcvData:

    """
    トレード用のOHLCVファイルを取得するクラスです。
    """

    def get_bitmex_ohlcv(self):
        """
        bitmexのohlcvデータを取得するメソッドです。
        :return: dict {"o":[],"h":[],"l":[],"c":[],"v":[]}
        """
        pass

    def get_cryptowatch_ohlcv(self):
        """
        cryptowatchからohlcvデータを取得するメソッドです。
        :return: dict {"o":[],"h":[],"l":[],"c":[],"v":[]}
        """
        pass

    def get_kupower_currency(self):
        """
        kupowerのデータを取得するメソッドです。
        :return: dict {"o":[],"h":[],"l":[],"c":[],"v":[]}
        """
        pass


class CreateStaticalStrategy(GetOhlcvData):

    """
    トレードを作成するクラスです。
    """
    def create_time_stacking_trend_strategy(self):
        """
        特定時刻(0-23時)の対数収益率を合計し、トレンドが発生している時点でトレードを行う
        戦略を作成するメソッドです。
        :return:
        """
        pass

    def create_week_and_time_stacking_trend_strategy(self):
        """
        特定の曜日(月~日)と特定時刻(0-23時)の対数収益率を合計し、トレンドが発生している時点でトレードを行う
        戦略を作成するメソッドです。
        :return:
        """

    def create_breakout_trend_strategy(self):
        """
        ブレイクアウト戦略において、特定のブレイクアウト後の特定時間の対数収益率が偏っている時点でトレードを行う
        戦略を作成するメソッドです。
        :return:
        """

    def create_brakeout_trend_with_ma_strategy(self):
        """
        ブレイクアウト戦略において、特定のブレイクアウト後の特定時間の対数収益率が偏っており、かつMA条件を付けた
        時点でトレードを行う戦略を作成するメソッドです。
        :return:
        """


class TradingBackTest():

    """
    作成したトレードでバックテストを行うクラスです。
    """


if __name__ == "__main__":
    # 単体テスト
    pass
    # 結合テスト


