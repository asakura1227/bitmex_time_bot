# encoding:utf-8
# BTIMEXで時間別トレードを行うBOTです。
import const
import os

# データベースとログファイルを削除する
if os.path.exists(const.db_path):
    os.remove(const.db_path)
if os.path.exists(const.log_path):
    os.remove(const.log_path)

import pandas as pd
import requests
import logger_init
import logging
import numpy as np
import matplotlib.pyplot as plt
logger = logging.getLogger(__name__)
import scipy.stats as stats
np.set_printoptions(threshold=np.nan)
from db.db_connect import *

# 再帰の決定上限1万回
import sys
sys.setrecursionlimit(10000)

class create_statical_strategy():

    """
    トレードの戦略を作成するクラスです。
    """
    def __init__(self,start_time,end_time):
        # BITMEXAPIURL
        self.mex_url = "https://www.bitmex.com/api/udf/history"
        # ファイル管理用の辞書
        self.dir_dict = {
            "sumdiff_data": "sumdiff",
            "sumdiff_ols": "sumdiff_ols",
            "sumdiff_dist": "sumdiff_dist",
            "difflog_hist": "difflog_hist",

            "other": "other"
        }
        # データベースコネクタ
        self.db_con = db_connect()
        self.db_con.delete_analyze()

        self.analyze_start_time = start_time
        self.analyse_end_time = end_time

    def get_strategy(self):

        """
        データベースからトレードする時間帯を取得し辞書形式で返します。
        :return:
        """
        results = self.db_con.get_analyze_result()
        strategy_dict = {}
        for result in results:
            if not result.week in strategy_dict.keys():
                strategy_dict[result.week] = {"BUY":[],"SELL":[]}
            if result.week in strategy_dict.keys():
                if result.sumdiff_ols_alpha > 0:
                    strategy_dict[result.week]["BUY"].append(result.hour)
                if result.sumdiff_ols_alpha < 0:
                    strategy_dict[result.week]["SELL"].append(result.hour)
        # トレード時間が出力
        print(strategy_dict)
        return strategy_dict

    def check_static_ols_dist(self,sm_result,id):
        """
        回帰分析の残差をチェックします
        :param series:
        :param id:
        :return:
        """
        # shapiro検定で回帰式の残差が正規分布に従っているかを調べる
        print("回帰式の残差が正規分布に従っているかどうかを調べる")
        dist = sm_result.resid
        print("歪度")
        skew = dist.skew()
        print(str(skew))
        print("尖度")
        kurt = dist.kurt()
        print(str(kurt))
        print("正規性の検定")
        shapiro = stats.shapiro(dist)
        shapiro_bool = False if shapiro[1] < 0.05 else True
        print(shapiro_bool)
        param = {
            "id":id,
            "sumdiff_ols_dist_skew":skew,
            "sumdiff_ols_dist_kurt":kurt,
            "sumdiff_ols_dist_sharpiro":shapiro_bool
        }
        # データベースの情報をアップデート
        self.db_con.update_analyze_result_second(param)


    def check_statics_diffsum(self, series,id):
        """
        渡されたpandas.seriesのDIFFSUMから統計的性質を計算し、確認します。
        :return:
        """
        # sumdiffのランダムウォーク検定を行う
        import statsmodels.api as sm
        # トレンド無のランダムウォーク検定
        # ランダムウォーク検定（トレンド無）
        p_value_no_trend = sm.tsa.adfuller(series,regression="nc")[1]
        # ランダムウォーク検定（トレンド有）
        p_value_timetrend = sm.tsa.adfuller(series, regression="ct")[1]
        # ランダムウォーク検定（標準）
        p_value_default = sm.tsa.adfuller(series, regression="c")[1]
        # 回帰分析を行う
        import statsmodels.api as sm
        y = series
        x = range(len(y))
        x = sm.add_constant(x)
        model = sm.OLS(y,x)
        sm_result = model.fit()
        # 回帰
        print(sm_result.summary())
        # 回帰の結果からデータ内容を取得
        print(sm_result.summary())
        a,b = sm_result.params
        print(str(a))
        print(str(b))
        # 回帰式を出力
        print("回帰式")
        print(str(round(a, 4))+"x+"+str(round(b, 4)))
        # 決定係数を出力
        print("決定係数")
        print(str(sm_result.rsquared))
        rsquared = round(sm_result.rsquared,4)
        print("調整済み決定係数")
        print(str(sm_result.rsquared_adj))
        adjusted_rsquared = round(sm_result.rsquared, 4)
        print("P値")
        print(str(sm_result.pvalues.const))
        ols_p_values = sm_result.pvalues.const
        # データベースのデータをアップデート
        param = {
            "id":id,
            "sumdiff_random_walk_notrend":p_value_no_trend,
            "sumdiff_random_walk_timetrend":p_value_timetrend,
            "sumdiff_random_walk_default":p_value_default,
            "sumdiff_ols_alpha":a,
            "sumdiff_ols_beta":b,
            "sumdiff_ols_rsquare":rsquared,
            "sumdiff_ols_adjusted_rsquare":adjusted_rsquared,
            "sumdiff_ols_p_value":ols_p_values,
        }
        # アップデート
        # 回帰のグラフを出力
        self.db_con.update_anaylze_result_first(param)
        return sm_result

    def check_statics_diff(self,series,id,week,hour):
        """
        引数で渡されたpandas.seriesから統計的性質を確認し、確認します。
        :param df:
        :return:
        """
        param = {}

        # ランダムウォーク検定を行う
        # 内容を出力
        print("--------------------------------------------------------------")
        print("LOGDIFFのランダムウォーク検定")

        import statsmodels.api as sm
        # トレンド無のランダムウォーク検定
        p_value_no_trend = sm.tsa.adfuller(series,regression="nc")[1]
        print("トレンド無のランダムウォーク検定")
        print(p_value_no_trend)
        # トレンド有のランダムウォーク検定
        print("時間トレンド有のランダムウォーク検定")
        p_value_timetrend = sm.tsa.adfuller(series,regression="ct")[1]
        print(p_value_timetrend)
        print("標準")
        p_value_default = sm.tsa.adfuller(series,regression="c")[1]
        print(p_value_default)
        # 基本統計量の算出(平均)
        print("基本統計量の算出")
        mean = series.mean()
        print("平均")
        print(str(mean))
        median = series.median()
        print("中央値")
        print(str(median))
        print("歪度")
        skew = series.skew()
        print(str(skew))
        print("突度")
        kurt = series.kurt()
        print(str(kurt))
        print("標準偏差")
        std = series.std()
        print(str(std))
        print("ヒストグラムのプロット")
        print("正規分布の検定")
        shapiro = stats.shapiro(series)
        shapiro_bool = False if shapiro[1] < 0.05 else True
        print(shapiro_bool)
        # 計算結果をデータベースに格納する
        param = {
            "id": id,
            "hour":hour,
            "week":week,
            "diff_log_random_walk_notrend": p_value_no_trend,
            "diff_log_random_walk_timetraned":p_value_timetrend,
            "diff_log_random_walk_default":p_value_default,
            "diff_log_mean":mean,
            "diff_log_median":median,
            "diff_log_skew":skew,
            "diff_log_kurt":kurt,
            "diff_log_std":std,
            "diff_log_shapiro":shapiro_bool,
        }
        self.db_con.add_analyze_result(param)


    def save_graph(self,series,graph_name):
        """
        引数で取得したシリーズをグラフにプロットし、画像を保存します
        :param series:
        :return:
        """

        # グラフ用フォルダがない場合に作成
        folder_name = ""
        for key in self.dir_dict.keys():
            if not os.path.exists(const.file_path + key):
                os.mkdir(const.file_path + key)
        for key in self.dir_dict.keys():
            if key in graph_name:
                folder_name = key
                break
        if folder_name == "":
            folder_name = "other"
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1)
        if "normal" in graph_name:
            # ノーマルのグラフをプロット
            ax.plot(series, label=folder_name, color="blue")
            ax.ylabel=folder_name
        elif "hist" in graph_name:
            # ヒストグラムのプロット
            ax.hist(series, label=folder_name, color="blue",bins=50)
        elif "ols" in graph_name:
            # 線形回帰のプロット
            # 最初に線形回帰を行い、回帰係数を取得する
            import statsmodels.api as sm
            y = series
            x = range(len(y))
            x = sm.add_constant(x)
            model = sm.OLS(y, x)
            sm_result = model.fit()
            # データをプロット
            ax.plot(series, label=folder_name, color="blue")
            ax.ylabel=folder_name
            # 線形回帰線をプロット
            sm_result.fittedvalues.plot(label="prediction",style="--")
        plt.savefig(const.file_path + folder_name + "\\" + graph_name + ".png")
        # メモリ上に展開された内容を削除する
        fig.clf()
        plt.close(fig)

    def get_mex_ohlcv(self):
        """
        BITMEXからohlcvファイルをダウンロードしてPandasデータフレームに変換します。
        :return:
        """
        param = {
            "symbol": "XBTUSD",
            "resolution": 60,
            "from": self.analyze_start_time,
            "to": self.analyse_end_time
        }
        result = requests.get(self.mex_url, param)
        logger.info(result)
        logger.info(result.url)
        mex_ohlcv = result.json()
        # ダウンロードしたOHLCVデータをPandasDataFrameに変換する
        # OHLCVのTimeStampを日付を時刻に分解する
        mex_time_list = []
        mex_date_list = []
        mex_week_list = []
        for mex_time_temp in mex_ohlcv["t"]:
            mex_time_temp_convert = datetime.datetime.fromtimestamp(mex_time_temp)
            mex_time_list.append(mex_time_temp_convert.time())
            mex_date_list.append(mex_time_temp_convert.date())
            mex_week_list.append(mex_time_temp_convert.weekday())
        # データフレーム作成
        df = pd.DataFrame(
            {
                "DATE": mex_date_list,
                "TIME": mex_time_list,
                "WEEKDAY": mex_week_list,
                "OPEN": mex_ohlcv["o"],
                "HIGH": mex_ohlcv["h"],
                "LOW": mex_ohlcv["l"],
                "CLOSE": mex_ohlcv["c"],
                "VOLUME": mex_ohlcv["v"]
            }
        )
        # 中身を確認
        print(df.head(100))
        return df

    def statical_calc(self):
        """
        BITMEXのデータからトレード戦略を計算して決定します。
        :return:
        """
        trading_time = {}
        df = self.get_mex_ohlcv()
        #無効なデータを削除
        df.dropna()
        # 各時間の対数差分を計算してDataFrameに追加する
        diff = np.log(df["CLOSE"]) - np.log(df["OPEN"])
        df["DIFFLOG"] = diff
        print(df.head(100))
        id = 0
        # 各時間毎にランダムウォーク検定、回帰分析、残差分析を行い分析結果をファイルに出力、トレードマップを作成する
        # 時間
        for hour in range(0,24): # 0時~24時まで
            #時刻を作成
            analyze_time = datetime.time(hour, 0, 0)
            for week in range(0,7): # 0(月曜日) ~ 6(日曜日)まで
                # データidを発行
                id += 1
                # 曜日を作成
                analyze_week = week
                # データフレームからデータを絞り込み
                filter_df = df[(df["TIME"] == analyze_time) & (df["WEEKDAY"] == week)]
                # データフレームにDIFFSUMを追加する
                diff_log_series = filter_df["DIFFLOG"]
                diff_log_series_list = diff_log_series.values.tolist()
                print("diff_log_series_list_len")
                print(str(len(diff_log_series_list)))
                sum_list = []
                for num in range(0, len(diff_log_series_list)):
                    if num == 0:
                        sum_list.append(diff_log_series_list[num])
                    else:
                        sum_list.append(diff_log_series_list[num] + sum_list[num-1])
                print("内容確認")
                print("sum_list_lens")
                print(str(len(sum_list)))
                # 内容をfilter_dfに追加
                sum_series = pd.Series(sum_list)
                # 内容確認
                print(sum_series.head(100))
                # 内容出力
                self.save_graph(sum_series,"sumdiff_data_week_" + str(week) + "_hour_" + str(hour) + "_normal")
                # 収益率の統計情報を書き出し
                self.check_statics_diff(filter_df["DIFFLOG"], id,week,hour)
                # ヒストグラムのグラフをプロットする
                self.save_graph(sum_series, "difflog_hist_week" + str(week)+"_hour_"+str(hour) + "hist")
                # 累積対数収益率の線形回帰の統計情報を書き出し
                sm_result = self.check_statics_diffsum(sum_series, id)
                # 累積対数収益率の線形回帰の結果をグラフにプロットする
                self.save_graph(sum_series, "sumdiff_ols_week" + str(week) + "_hour_" + str(hour) + "ols")
                # 累積対数収益率の線形回帰の差分をグラフにプロットする
                self.save_graph(sm_result.resid,"sumdiff_dist_week" + str(week) + "_hour_" + str(hour) + "hist")
                # 累積対数収益率の統計情報を書き出し
                self.check_static_ols_dist(sm_result,id)
        # # トレードする時間をデータベースから取得する
        strategy_dict = self.get_strategy()
        print("Anayzing Process Is Completed")
        return strategy_dict


class statical_trading():
    """
    統計的時間別トレンドに従いトレードを行うクラスです。
    """
    def __init__(self):
        """
        初期処理
        """
        # BITMEXAPIURL
        self.mex_url = "https://www.bitmex.com/api/udf/history"
        self.db_con = db_connect()
    def run_forward_test_with_moving_analyze_time(self):
        """
         指定期間でフォワードテストを行うメソッドです。
         1か月に一回ストラテジを更新します。
         :return:
         """
        profit_dict = {}
        for num in range (180,181,1):
            # 定数
            TIME = "t"
            OPEN = "o"
            CLOSE = "c"
            profit = []
            # バックテスト終了日時
            back_test_end_time = int(datetime.datetime.now().timestamp())
            # トレード時刻作成用データ取得開始日時
            strategy_start_time = back_test_end_time - 10080 * 60 * 60
            # トレード時刻作成用データ取得終了日時
            strategy_end_time = strategy_start_time + 60 * 60 * 24 * num
            # バックテスト開始時刻
            back_test_start_time = strategy_end_time
            # バックテスト用のOHLCVデータを取得する
            param = {
                "symbol":const.symbol,
                "resolution": 60,
                "from": back_test_start_time,
                "to": back_test_end_time
            }
            bitmex_ohlcv = requests.get(self.mex_url, param).json()
            # バックテスト時間を取得する
            create_strategy = create_statical_strategy(strategy_start_time, strategy_end_time)
            strategy_time = create_strategy.statical_calc()
            # バックテスト開始
            i = 0
            lose_count = 0
            trading_id = 0
            for i in range(0, len(bitmex_ohlcv[TIME])):

                ohlcv_datetime = datetime.datetime.fromtimestamp(bitmex_ohlcv[TIME][i])
                if (ohlcv_datetime.day == 1) and ohlcv_datetime.hour == 0:
                    print("毎月1日にストラテジを更新する")
                    # 経過時間を抽出
                    passed_time = bitmex_ohlcv[TIME][i] - back_test_start_time
                    print("passd_time")
                    print(str(passed_time))
                    # 結果時間をストラテジ生成時間に足す
                    strategy_start_time = bitmex_ohlcv[TIME][i] - 60 * 60 * 24 * num
                    strategy_end_time = bitmex_ohlcv[TIME][i - 1]
                    print("ストラテジを生成時間")
                    print(datetime.datetime.fromtimestamp(strategy_start_time))
                    print(datetime.datetime.fromtimestamp(strategy_end_time))
                    # ストラテジを取得する
                    create_strategy = create_statical_strategy(strategy_start_time, strategy_end_time)
                    strategy_time = create_strategy.statical_calc()
                week = ohlcv_datetime.weekday()
                hour = ohlcv_datetime.hour
                print("WEEKDAY:" + str(week) + " TIME:" + str(hour))
                if week in strategy_time.keys():
                    # 買トレードを行う
                    if hour in strategy_time[week]["BUY"]:
                        param = {
                            "id": trading_id,
                            "in_price": bitmex_ohlcv[OPEN][i],
                            "in_time": bitmex_ohlcv[TIME][i],
                            "exit_price": bitmex_ohlcv[CLOSE][i],
                            "exit_time": bitmex_ohlcv[TIME][i],
                            "trading_direction": "買",
                            "profit": bitmex_ohlcv[CLOSE][i] - bitmex_ohlcv[OPEN][i],
                            "lot": 1,
                            "lose_count": lose_count,
                        }
                        print("BUY")
                        print(param)
                        self.db_con.add_trading(param)
                        # トレードID+1
                        trading_id += 1
                        # 負けたときに負け回数+1
                        # 利益をprofitリストに追加
                        profit.append(bitmex_ohlcv[CLOSE][i] - bitmex_ohlcv[OPEN][i])
                        if bitmex_ohlcv[CLOSE][i] - bitmex_ohlcv[OPEN][i] > 0:
                            lose_count = 0
                        else:
                            lose_count += 1
                    # 売トレードを行う
                    elif hour in strategy_time[week]["SELL"]:
                        param = {
                            "id": trading_id,
                            "in_price": bitmex_ohlcv[OPEN][i],
                            "in_time": bitmex_ohlcv[TIME][i],
                            "exit_price": bitmex_ohlcv[CLOSE][i],
                            "exit_time": bitmex_ohlcv[TIME][i],
                            "trading_direction": "売",
                            "profit": bitmex_ohlcv[OPEN][i] - bitmex_ohlcv[CLOSE][i],
                            "lot": 1,
                            "lose_count": lose_count,
                        }
                        profit.append(bitmex_ohlcv[OPEN][i] - bitmex_ohlcv[CLOSE][i])
                        print("SELL")
                        print(param)
                        self.db_con.add_trading(param)
                        # トレードID+1
                        trading_id += 1
                        # 負けたときに負け回数+1
                        if bitmex_ohlcv[OPEN][i] - bitmex_ohlcv[CLOSE][i] > 0:
                            lose_count = 0
                        else:
                            lose_count += 1
            print("BackTest Process Moving BackTest Is Completed" + ".." + str(num))
            # バックテストの利益を辞書に格納
            profit_dict[num] = sum(profit)
            # 全ての取引データを削除
            self.db_con.delete_trading()
        print("BackTest All Process Is Completed")
        print(profit_dict)
        max_profit_key = max(profit_dict.items(), key=lambda x:x[1])[0]
        print(str(max_profit_key))

    def run_forward_test_with_time(self):
        """
        指定期間でフォワードテストを行うメソッドです。
        1ヶ月に一回ストラテジを更新します。
        :return:
        """
        # 定数
        TIME = "t"
        OPEN ="o"
        CLOSE ="c"

        # バックテスト終了日時
        back_test_end_time = int(datetime.datetime.now().timestamp())
        # トレード時刻作成用データ取得開始日時
        strategy_start_time = back_test_end_time - 10080 * 60 * 60
        # トレード時刻作成用データ取得終了日時
        strategy_end_time = strategy_start_time + 60 * 60 * 24 * const.data_days
        # バックテスト開始時刻
        back_test_start_time = strategy_end_time
        # バックテスト用のOHLCVデータを取得する
        param = {
            "symbol": "XBTUSD",
            "resolution": 60,
            "from": back_test_start_time,
            "to": back_test_end_time
        }
        bitmex_ohlcv = requests.get(self.mex_url,param).json()
        # バックテスト時間を取得する
        create_strategy = create_statical_strategy(strategy_start_time,strategy_end_time)
        strategy_time = create_strategy.statical_calc()
        # バックテスト開始
        i = 0
        lose_count = 0
        trading_id = 0
        for i in range(0,len(bitmex_ohlcv[TIME])):

            ohlcv_datetime = datetime.datetime.fromtimestamp(bitmex_ohlcv[TIME][i])
            if (ohlcv_datetime.day == 1) and ohlcv_datetime.hour == 0:
                print("毎月1日にストラテジを更新する")
                # 経過時間を抽出
                passed_time = bitmex_ohlcv[TIME][i] - back_test_start_time
                print("passd_time")
                print(str(passed_time))
                # 結果時間をストラテジ生成時間に足す
                strategy_start_time = bitmex_ohlcv[TIME][i] - 60 * 60 * 24 * const.data_days
                strategy_end_time = bitmex_ohlcv[TIME][i-1]
                print("ストラテジを生成時間")
                print(datetime.datetime.fromtimestamp(strategy_start_time))
                print(datetime.datetime.fromtimestamp(strategy_end_time))
                # ストラテジを取得する
                create_strategy = create_statical_strategy(strategy_start_time, strategy_end_time)
                strategy_time = create_strategy.statical_calc()
            week = ohlcv_datetime.weekday()
            hour = ohlcv_datetime.hour
            print("WEEKDAY:"+str(week)+" TIME:"+str(hour))
            if week in strategy_time.keys():
                # 買トレードを行う
                if hour in strategy_time[week]["BUY"]:
                    param = {
                        "id": trading_id,
                        "in_price":bitmex_ohlcv[OPEN][i],
                        "in_time":bitmex_ohlcv[TIME][i],
                        "exit_price":bitmex_ohlcv[CLOSE][i],
                        "exit_time":bitmex_ohlcv[TIME][i],
                        "trading_direction":"買",
                        "profit":bitmex_ohlcv[CLOSE][i] - bitmex_ohlcv[OPEN][i],
                        "lot":1,
                        "lose_count":lose_count,
                        }
                    print("BUY")
                    print(param)
                    self.db_con.add_trading(param)
                    # トレードID+1
                    trading_id +=1
                    # 負けたときに負け回数+1
                    if bitmex_ohlcv[CLOSE][i] - bitmex_ohlcv[OPEN][i] > 0:
                        lose_count = 0
                    else:
                        lose_count +=1
                # 売トレードを行う
                elif hour in strategy_time[week]["SELL"]:
                    param = {
                        "id": trading_id,
                        "in_price":bitmex_ohlcv[OPEN][i],
                        "in_time":bitmex_ohlcv[TIME][i],
                        "exit_price":bitmex_ohlcv[CLOSE][i],
                        "exit_time":bitmex_ohlcv[TIME][i],
                        "trading_direction":"売",
                        "profit":bitmex_ohlcv[OPEN][i] - bitmex_ohlcv[CLOSE][i],
                        "lot":1,
                        "lose_count":lose_count,
                        }
                    print("SELL")
                    print(param)
                    self.db_con.add_trading(param)
                    # トレードID+1
                    trading_id+=1
                    # 負けたときに負け回数+1
                    if bitmex_ohlcv[OPEN][i] - bitmex_ohlcv[CLOSE][i] > 0:
                        lose_count = 0
                    else:
                        lose_count += 1
        print("BackTest Process Moving BackTest Is Completed")
    def run_foward_test(self):
        """
        指定期間でフォワードテストを行うメソッドです。
        単純フォワードテスト
        """
        # 定数
        TIME = "t"
        OPEN ="o"
        CLOSE ="c"

        # バックテスト終了日時
        back_test_end_time = int(datetime.datetime.now().timestamp())
        # トレード時刻作成用データ取得開始日時
        strategy_start_time = back_test_end_time - 10080 * 60 * 60
        # トレード時刻作成用データ取得終了日時
        strategy_end_time = strategy_start_time + 60 * 60 * 24 * const.data_days
        # バックテスト開始時刻
        back_test_start_time = strategy_end_time
        # バックテスト用のOHLCVデータを取得する
        param = {
            "symbol": "XBTUSD",
            "resolution": 60,
            "from": back_test_start_time,
            "to": back_test_end_time
        }
        bitmex_ohlcv = requests.get(self.mex_url,param).json()
        # バックテスト時間を取得する
        create_strategy = create_statical_strategy(strategy_start_time,strategy_end_time)
        strategy_time = create_strategy.statical_calc()
        # バックテスト開始
        i = 0
        lose_count = 0
        trading_id = 0
        for i in range(0,len(bitmex_ohlcv[TIME])):
            ohlcv_datetime = datetime.datetime.fromtimestamp(bitmex_ohlcv[TIME][i])
            week = ohlcv_datetime.weekday()
            hour = ohlcv_datetime.hour
            print("WEEKDAY:"+str(week)+" TIME:"+str(hour))
            if week in strategy_time.keys():
                # 買トレードを行う
                if hour in strategy_time[week]["BUY"]:
                    param = {
                        "id": trading_id,
                        "in_price":bitmex_ohlcv[OPEN][i],
                        "in_time":bitmex_ohlcv[TIME][i],
                        "exit_price":bitmex_ohlcv[CLOSE][i],
                        "exit_time":bitmex_ohlcv[TIME][i],
                        "trading_direction":"買",
                        "profit":bitmex_ohlcv[CLOSE][i] - bitmex_ohlcv[OPEN][i],
                        "lot":1,
                        "lose_count":lose_count,
                        }
                    print("BUY")
                    print(param)
                    self.db_con.add_trading(param)
                    # トレードID+1
                    trading_id +=1
                    # 負けたときに負け回数+1
                    if bitmex_ohlcv[CLOSE][i] - bitmex_ohlcv[OPEN][i] > 0:
                        lose_count = 0
                    else:
                        lose_count +=1
                # 売トレードを行う
                elif hour in strategy_time[week]["SELL"]:
                    param = {
                        "id": trading_id,
                        "in_price":bitmex_ohlcv[OPEN][i],
                        "in_time":bitmex_ohlcv[TIME][i],
                        "exit_price":bitmex_ohlcv[CLOSE][i],
                        "exit_time":bitmex_ohlcv[TIME][i],
                        "trading_direction":"売",
                        "profit":bitmex_ohlcv[OPEN][i] - bitmex_ohlcv[CLOSE][i],
                        "lot":1,
                        "lose_count":lose_count,
                        }
                    print("SELL")
                    print(param)
                    self.db_con.add_trading(param)
                    # トレードID+1
                    trading_id+=1
                    # 負けたときに負け回数+1
                    if bitmex_ohlcv[OPEN][i] - bitmex_ohlcv[CLOSE][i] > 0:
                        lose_count = 0
                    else:
                        lose_count += 1
        print("BackTest Process Normal BackTest Is Completed")

if __name__ == "__main__":
    trading = statical_trading()
    # 単純フォワードテスト
    # trading.run_foward_test()
    # 時間追従フォワードテスト
    #trading.run_forward_test_with_time()
    trading.run_forward_test_with_time()



