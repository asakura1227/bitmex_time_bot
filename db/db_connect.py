# encoding:utf-8
from db.db_models import *
import logging
import datetime
import logger_init

logger = logging.getLogger(__name__)

#テーブルを作成、存在する場合は作成しない
db.create_tables([Trading, Analayze_Result])
logging.debug("データベースのテーブルを作成する")

class db_connect():

    def add_analyze_result(self,param):

        """
        統計情報をデータベースに保存するメソッドです。
        :param param:
        :return: True:完了 False:失敗
        """
        now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        try:
            if isinstance(param, list):
                return False
            elif isinstance(param, dict):
                param["update_time"] = now_time
                param["create_time"] = now_time
            else:
                logger.debug("引数の型が異常です")
                return False

            with db.transaction():
                # データをインサート
                Analayze_Result.insert_many(param).execute()
                db.commit()
                return True
        except IntegrityError as ex:
            logging.exception(ex)
            db.rollback()
            return False

    def get_analyze_result(self):

        """
        トレード条件に合致するデータを取得します。
        トレード条件：回帰決定係数 R > 0.7
        トレード条件：回帰係数の残差が正規分布
        :param id:
        :return:
        """

        try:
            with db.transaction():
                results = Analayze_Result.select().where(Analayze_Result.sumdiff_ols_dist_sharpiro == 1,
                                                        Analayze_Result.sumdiff_ols_rsquare > 0.8,
                                                        Analayze_Result.diff_log_random_walk_default < 0.05,
                                                        Analayze_Result.diff_log_shapiro == 0,
                                                        )
                if len(results) == 0 or results is None:
                    return None
                else:
                    return results
        except IntegrityError as ex:
            logger.exception(ex)
            db.rollback()
            return None


    def update_anaylze_result_first(self,param):

        """
        データベースの統計情報を更新するメソッドです。(第一段階）
        :param param:
        :return: True:完了 False:失敗
        """
        try:
            with db.transaction():
                # idからデータを取得する。
                analyze = Analayze_Result.get(Analayze_Result.id == param["id"])
                analyze.sumdiff_random_walk_notrend = param["sumdiff_random_walk_notrend"]
                analyze.sumdiff_random_walk_timetrend = param["sumdiff_random_walk_timetrend"]
                analyze.sumdiff_random_walk_default = param["sumdiff_random_walk_default"]
                analyze.sumdiff_ols_alpha = param["sumdiff_ols_alpha"]
                analyze.sumdiff_ols_beta = param["sumdiff_ols_beta"]
                analyze.sumdiff_ols_rsquare = param["sumdiff_ols_rsquare"]
                analyze.sumdiff_ols_adjusted_rsquare = param["sumdiff_ols_adjusted_rsquare"]
                analyze.sumdiff_ols_p_value = param["sumdiff_ols_p_value"]
                # 更新時間を更新
                analyze.update_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                analyze.save()
                return True
        except IntegrityError as ex:
            logger.exception(ex)
            db.rollback()
            return False

    def update_analyze_result_second(self,param):

        """
        データベースの統計情報を更新するメソッドです。(第一段階）
        :param param:
        :return: True:完了 False:失敗
        """
        try:
            with db.transaction():
                # idからデータを取得する。
                analyze = Analayze_Result.get(Analayze_Result.id == param["id"])
                analyze.sumdiff_ols_dist_skew = param["sumdiff_ols_dist_skew"]
                analyze.sumdiff_ols_dist_kurt = param["sumdiff_ols_dist_kurt"]
                analyze.sumdiff_ols_dist_sharpiro = param["sumdiff_ols_dist_sharpiro"]
                # 更新時間を更新
                analyze.update_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                analyze.save()
                return True
        except IntegrityError as ex:
            logger.exception(ex)
            db.rollback()
            return False
    def delete_analyze(self):
        """
        テーブルの情報を全て削除するメソッドです。
        :return:
        """
        try:
            with db.transaction():
                # idからデータを取得する。
                query = Analayze_Result.delete()
                query.execute()
                return True
        except IntegrityError as ex:
            logger.exception(ex)
            db.rollback()
            return False


    def add_trading(self, param):

        """
        トレード情報をデータベースに追加するメソッドです。
        :param param: 登録するデータベース情報
        :return:  True：完了　False：失敗
        """

        now_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

        try:
            if isinstance(param, list):
                return False
            elif isinstance(param, dict):
                param["update_time"] = now_time
                param["create_time"] = now_time
            else:
                logger.debug("引数の型が異常です")
                return False

            with db.transaction():
                # データをインサート
                Trading.insert_many(param).execute()
                db.commit()
                return True
        except IntegrityError as ex:
            logging.exception(ex)
            db.rollback()
            return False

    # 指定idからトレード情報を取得する
    def get_trading_byid(self, id):

        """
        指定したらidのトレード情報を取得します。
        :param id: 取得するトレードID
        :return:
        """
        try:
            with db.transaction():
                result = Trading.select().where(Trading.id == id)
                if len(result) == 0 or result is None:
                    return None
                return result[0]
        except IntegrityError as ex:
            logger.exception(ex)
            db.rollback()
            return None

    # 最後のトレード情報を取得
    def get_trading_latest(self):

        """
        最後に追加された更新されたトレード情報を取得します。
        最後のトレード
        :return:
        """
        try:
            with db.transaction():
                result = Trading.select().order_by(
                    Trading.id.desc())
                if len(result) == 0 or result is None:
                    return None
                return result[0]
        except IntegrityError as ex:
            logger.exception(ex)
            db.rollback()
            return None

    # トレード情報の更新
    def update_trading(self,param):

        """
        データベースのトレード情報を更新します。
        :param param: id を必ず指定
        :return: False : 失敗　True : 成功
        """

        # リストもしくはdictにidが含まれていない場合エラーで返す
        if not isinstance(param,dict):
            logger.debug("引数が辞書形式ではありません。")
            return False
        if "id" not in param:
            logger.debug("引数の中にidが含まれていません。")
            return False

        logger.debug("アップデート内容をチェック")
        logger.debug(param)

        try:
            with db.transaction():
                # idからデータを取得する。
                trading = Trading.get(Trading.id == param["id"])
                trading.exit_price = param["exit_price"]
                trading.exit_time = param["exit_time"]
                trading.lose_count = param["lose_count"]

                # 利益を計算してインサート
                if trading.trading_direction == "buy":
                    trading.profit = param["exit_price"] - trading.in_price
                elif trading.trading_direction == "sell":
                    trading.profit = trading.in_price - param["exit_price"]

                # 更新時間を更新
                trading.update_time = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                trading.save()

                return True
        except IntegrityError as ex:
            logger.exception(ex)
            db.rollback()
            return False

    def delete_trading(self):

        """
         テーブルの情報を全て削除するメソッドです。
         :return:
         """
        try:
            with db.transaction():
                # idからデータを取得する。
                query = Trading.delete()
                query.execute()
                return True
        except IntegrityError as ex:
            logger.exception(ex)
            db.rollback()
            return False
