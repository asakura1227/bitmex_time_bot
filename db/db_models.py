# encoding:utf-8
# データベースのモデル定義を行うクラス
from peewee import *
import const
db = SqliteDatabase(const.db_path)

# 分析結果格納テーブル
class Analayze_Result(Model):

    # 分析結果ID
    id = IntegerField(primary_key=True, null=False, unique=True)
    # 時刻
    hour = TimeField(null=False)
    # 曜日
    week = IntegerField(null=False)
    # diff_logのランダムウォーク検定（トレンド無し）- P_Value
    diff_log_random_walk_notrend = FloatField(null=False)
    # diff_logのランダムウォーク検定 (トレンド有） - P_Value
    diff_log_random_walk_timetraned = FloatField(null=False)
    # diff_logのランダムウォーク検定（標準）- P_Value
    diff_log_random_walk_default = FloatField(null=False)
    # diff_logの基本統計量(平均）
    diff_log_mean = FloatField(null=False)
    # diff_logの基本統計量(中央値)
    diff_log_median = FloatField(null=False)
    # diff_logの基本統計量(歪度）
    diff_log_skew = FloatField(null=False)
    # diff_logの基本統計量(突度）
    diff_log_kurt = FloatField(null=False)
    # diff_logの基本統計量(標準偏差)
    diff_log_std = FloatField(null=False)
    # diff_logの正規分布検定
    diff_log_shapiro = BooleanField(null=False)
    # sumdiffのランダムウォーク検定（トレンド無) - P_Value
    sumdiff_random_walk_notrend = FloatField(null=True)
    # sumdiffのランダムウォーク検定（トレンド有) - P_Value
    sumdiff_random_walk_timetrend = FloatField(null=True)
    # sunmdiffのランダムウォーク検定（標準) - P_Value
    sumdiff_random_walk_default = FloatField(null=True)
    # sumdiffの回帰式（α）
    sumdiff_ols_alpha = FloatField(null=True)
    # sumdiffの回帰式（β）
    sumdiff_ols_beta = FloatField(null=True)
    # sumdiffの回帰式決定係数
    sumdiff_ols_rsquare = FloatField(null=True)
    # sumdiffの回帰式調整済み決定係数
    sumdiff_ols_adjusted_rsquare = FloatField(null=True)
    # sumdiffの回帰式のP値
    sumdiff_ols_p_value = FloatField(null=True)
    # sumdiffの回帰式の残差の歪度
    sumdiff_ols_dist_skew = FloatField(null=True)
    # sumdiffの回帰式の残差の突度
    sumdiff_ols_dist_kurt = FloatField(null=True)
    # sumdiffの回帰式の残差shapiro検定のp値
    sumdiff_ols_dist_sharpiro = FloatField(null=True)
    # 更新時間
    update_time = DateTimeField(null=False)
    # 作成時間
    create_time = DateTimeField(null=False)

    class Meta:
        database = db

# 取引情報テーブル
class Trading (Model):

    # トレードID
    id = IntegerField(primary_key=True, null=False, unique=True)
    # イン価格
    in_price = FloatField(null=False)
    # イン時間
    in_time = DateTimeField(null=False)
    # エクジット価格
    exit_price = FloatField(null=False)
    # エクジット時間
    exit_time = DateTimeField(null=False)
    # 売り or 買い
    trading_direction = CharField(null=False, max_length=4)
    # 利益
    profit = FloatField(null=False)
    # ロット
    lot = IntegerField(null=False)
    # 連続負け回数
    lose_count = IntegerField(null=False)
    # 更新時間
    update_time = DateTimeField(null=False)
    # 作成時間
    create_time = DateTimeField(null=False)

    class Meta:
        database = db
