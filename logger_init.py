# encoding:utf-8
# ログファイルを生成、書き込みするメソッドです。
import logging
import const
logging.basicConfig(filename=const.log_path,
                    level=logging.INFO, format="%(asctime)s %(funcName)s %(levelname)-7s %(message)s")
